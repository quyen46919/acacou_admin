const $ = document.querySelector.bind(document);
const $$ = document.querySelectorAll.bind(document);

const header = $('#header');
const menuMobile = $('.header__menu-mobile');
const btnBar = $('.header__btn-bar');
const wrapper = $('.header__wrapper');
const overlay = $('.header__overlay');
const btnNotify = $('#notify');
const notifyDropdown = $('.header__notify');
const tabsNotify = $$('.header__notify-tab');
const tabsContentNotify = $$('.header__notify-tab-content');
const btnUser = $('.header__user-mobile-icon');
const userMenuMobile = $('.header__user-mobile-list');
const userContainerMobile = $('.header__user-mobile-container');
const btnUserClose = $('.header__user-mobile-btn-close');
const userOverlay = $('.header__user-mobile-overlay');

const handleChangeOverflowBody = (overflow) => (document.body.style.overflow = `${overflow}`);

function modifyClass(selectors, classNames, action) {
  if (!Array.isArray(selectors) || !Array.isArray(classNames) || typeof action !== 'string') {
    throw new TypeError('Invalid arguments. Expected arrays for selectors and classNames, and a string for action.');
  }

  if (selectors.length !== classNames.length) {
    throw new Error('The number of selectors and classNames must be equal.');
  }

  switch (action) {
    case 'add':
      selectors.forEach((selector, index) => selector.classList.add(classNames[index]));
      break;
    case 'remove':
      selectors.forEach((selector, index) => selector.classList.remove(classNames[index]));
      break;
    case 'toggle':
      selectors.forEach((selector, index) => selector.classList.toggle(classNames[index]));
      break;
    default:
      throw new Error('Invalid action specified.');
  }
}

let isFirstClick = true;

btnUser.onclick = () => {
  if (isFirstClick) {
    modifyClass([btnUser, userMenuMobile], ['change', 'show'], 'add');
    isFirstClick = false;
  } else {
    modifyClass([userContainerMobile], ['change'], 'add');
  }
};

btnUserClose.onclick = () => {
  isFirstClick = true;
  modifyClass([btnUser, userMenuMobile, userContainerMobile], ['change', 'show', 'change'], 'remove');
};

userOverlay.onclick = () => {
  isFirstClick = true;
  modifyClass([btnUser, userMenuMobile, userContainerMobile], ['change', 'show', 'change'], 'remove');
};

btnNotify.onclick = (e) => {
  e.stopPropagation();
  notifyDropdown.classList.toggle('show');
};

window.onclick = (e) => {
  if (!notifyDropdown.contains(e.target)) notifyDropdown.classList.remove('show');
};

tabsNotify.forEach((tab) => {
  tab.onclick = function () {
    const tabId = this.getAttribute('data-notify-tab');
    const tabContentActive = $(`#${tabId}`);

    tabsNotify.forEach((tab) => tab.classList.remove('active'));
    tabsContentNotify.forEach((tabContent) => tabContent.classList.remove('active'));

    this.classList.add('active');
    tabContentActive.classList.add('active');
  };
});

let isOverflowHidden = false;

btnBar.onclick = () => {
  isOverflowHidden = !isOverflowHidden;
  handleChangeOverflowBody(isOverflowHidden ? 'hidden' : '');
  modifyClass([menuMobile, header, btnBar, overlay], ['open', 'open', 'active', 'show'], 'toggle');
};
overlay.onclick = () => {
  handleChangeOverflowBody('');
  modifyClass([menuMobile, header, btnBar, overlay], ['open', 'open', 'active', 'show'], 'remove');
};
